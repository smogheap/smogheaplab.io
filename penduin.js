let PENDUIN = {
	widget: {
		fancyLink: {
			evolve: function(a) {
				let file = a.id || "temp";
				let bg = "#ddd";
				if(file.indexOf(";") >= 0) {
					bg = file.split(";")[1];
					file = file.split(";")[0];
				}
				let label = document.createElement("label");
				//label.appendChild(document.createTextNode(a.innerText));
				label.innerHTML = a.innerHTML;
				while(a.firstChild) {
					a.removeChild(a.firstChild);
				}
				if(a.style) {
					a.style.backgroundColor = bg;
				} else {
					console.log("something weird", a);
				}
				for(let i = 0; i < 4; ++i) {
					let img = document.createElement("img");
					img.src="images/" + file + i + ".gif";
					img.className = "pos" + i;
					a.appendChild(img);
				}
				a.appendChild(label);
			}
		}
	}
};

window.addEventListener("load", function() {
	for(let widg in PENDUIN.widget) {
		let elms = document.querySelectorAll("." + widg);
		for(let k in Object.keys(elms)) {
			PENDUIN.widget[widg].evolve(elms[k]);
		}
	}
});
